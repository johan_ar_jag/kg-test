<?php

namespace Mastering\Labels\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class TrueAttributeViewModel implements ArgumentInterface
{
    protected $eavConfig;

    public function __construct(
        \Magento\Eav\Model\Config $eavConfig
    )
    {
        $this->eavConfig = $eavConfig;
    }

    public function getLiteralAttr($numericOptions)
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', 'labels');
        $options = $attribute->getSource()->getAllOptions();
        $result = [];
        foreach ($options as $literalOption) {
            foreach ($numericOptions as $numericOption) {
                if ($numericOption == $literalOption['value']) {
                    $result[] = $literalOption['label'];
                }
            }
        }
        return $result;
    }

    public function colorLabel($label)
    {
        $colorClass = "johan-label-" . strtolower(implode("-", explode(" ", trim($label))));
        return $colorClass;
    }
}