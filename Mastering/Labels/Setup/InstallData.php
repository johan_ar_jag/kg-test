<?php

namespace Mastering\Labels\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;

class InstallData implements InstallDataInterface
{

    private $eavSetupFactory;

    public function __construct(
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'labels',
            [
                'group' => 'General',
                'type' => 'text',
                'label' => 'Labels',
                'input' => 'multiselect',
                'option' => ['values' => [
                    'Sale',
                    'Free Shipping',
                    'Best Seller',
                ],],
                'backend' => \Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend::class,
                'required' => false,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'searchable' => true,
                'filterable' => true,
                'used_for_promo_rule' => true,
                'used_in_product_listing' => true,
                'used_for_sort_by' => true,
                'is_html_allowed_on_front' => true,
                'visible' => true,
                'visible_on_front' => true
            ]
        );
        $setup->endSetup();
    }
}